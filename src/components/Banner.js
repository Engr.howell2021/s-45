import React from 'react';
import {Row, Carousel} from 'react-bootstrap';


export default function Banner () {

    return (
        <Row>
				<Carousel>
  <Carousel.Item interval={3000}>
    <img
     className="d-block w-100" 
     src={require("../images/2.jpg")} 
     alt="First slide" 
     width="450px" 
     height="550px"
    />
  </Carousel.Item>

  <Carousel.Item interval={2000}>
    <img
     className="d-block w-100" 
     src={require("../images/1.jpg")} 
     alt="Second slide" 
     width="450px" 
     height="550px"
    />
  </Carousel.Item>

  <Carousel.Item interval={2000}>
    <img
     className="d-block w-100" 
     src={require("../images/3.jpg")} 
     alt="Third slide" 
     width="450px" 
     height="550px"
    />
  </Carousel.Item>
</Carousel>
			</Row>
		

    )
}